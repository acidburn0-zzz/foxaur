# FoxAUR - Contributing

## Table of contents
* [How to start](#How-to-start)
* [Development process](#Development-process)
    * [Branches](#Branches)
    * [Commits](#Commits)
    * [Merge requests](#Merge-requests)
    * [Release new version](#Release-new-version)

## How to start

Firstly check [list of issues](https://gitlab.com/foxaur/foxaur/issues). After you select issue, assign it to yourself. You can too add a new issue.

Before you start coding check [development process](#Development-process). There are important informations about naming conventions and merge request process.

## Development process

### Branches

Branch `master` is freezed branch with latest release, Release branch (for example `0.0.1`) is current status of new release.

All development branches needed to be checkout from release branch. Naming convention for this branches is:

```
#Issue_number-short_description
```

Spaces must be replaced by dash (`-`).

### Commits

Commiting is only allowed to development branches. Naming convention for commits is:

```
#Issue_number short_description
```

Spaces is allowed here. Description will contain information about changes included in commit.

Exception is merge commits. Merge commits naming convention is explained in [merge requests](#Merge-requests) section.

### Merge requests

All development branches needed to be merge to release branch. Naming convention for merge request is:

```
Resolve #issue_number - short_description
```

Spaces is allowed here. Description will contain information about changes included in merge request.

Try to merge as little commits as possible.

Merge commit naming convention is:

```
Merge #issue_number into release_branch_name
```

Merging is allowed only when [pipeline](https://gitlab.com/foxaur/foxaur/pipelines) passed, and when merge request have at least one approve.

### Release new version

When all issues in milestone is resolved, release branch can be merged to master. Naming convention for merge request is:

```
Release release_branch_name
```

Merge commit naming convention is:

```
Merge release_branch_name into master
```

Merging is allowed only when [pipeline](https://gitlab.com/foxaur/foxaur/pipelines) passed, and when merge request have at least one approve.

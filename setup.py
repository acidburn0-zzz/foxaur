import setuptools
import unittest

from foxaur import __version__


def test_suite():
    test_loader = unittest.TestLoader()
    suite = test_loader.discover("tests")
    return suite


with open ("README.md") as readme:
    long_description = readme.read()

setuptools.setup(
    name="FoxAUR",
    version=__version__,
    author="Konrad Partas",
    author_email="konparta@gmail.com",
    description="Pacman wrapper and AUR helper to keep it simple.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/foxaur/foxaur",
    packages=setuptools.find_packages(),
    keywords="pacman aur",
    test_suite="setup.test_suite",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: GNU GPLv3 License",
        "Operating System :: Linux",
    ],
    entry_points={
        "console_scripts": [
            "foxaur = foxaur.__main__:main"
        ]
    }
)

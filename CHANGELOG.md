# FoxAUR - Changelog

## [0.0.1](https://gitlab.com/foxaur/foxaur/tags/0.0.1) - 30.09.2018

### Added

* Ability to install packages, remove, upgrade and list packages
* AUR support
* Upgrade all packages
* No dependencies flag for removing packages
* Ability to list all installed packages

# FoxAUR

[![pipeline status](https://gitlab.com/foxaur/foxaur/badges/master/pipeline.svg)](https://gitlab.com/foxaur/foxaur/commits/master)

**Pacman wrapper and AUR helper to keep it simple.**

The idea is to replace hard to understand for beginners pacman flags like `-Syu` with simpler arguments.

## Table of contents

* [Features](#Features)
* [Dependencies](#Dependencies)
* [Installation](#Installation)
    * [By PKGBUILD](#By-PKGBUILD)
    * [Manually](#Manually)
* [Usage](#Usage)
    * [Install package](#Install-package)
    * [Remove package](#Remove-package)
        * [With dependiences](#With-dependiences)
        * [Without dependiences](#Without-dependiences)
    * [Update all packages](#Update-all-packages)
    * [List installed packages](#List-installed-packages)
        * [List all packages](#List-all-packages)
        * [List specific package](#List-specific-package)
* [Contributing](#Contributing)
* [Changelog](#Changelog)
* [Licence](#Licence)

## Features

* Replaces pacman command by simple arguments
* Installs AUR packages by searching AUR repository
* Take configuration from pacman.conf
* Ability to work on multiple packages (from repositories and AUR)

## Dependencies

* Arch Linux based system
* Python 3
* Pacman
* Git

All dependencies will be automatically installed when you run [installation by PKGBUILD]($By-PKGBUILD).

## Installation

### By PKGBUILD
```bash
git clone https://gitlab.com/foxaur/foxaur.git
cd foxaur
makepkg -sic
```

### Manually
```bash
git clone https://gitlab.com/foxaur/foxaur.git
python setup.py install
```

## Usage

### Install package

```bash
foxaur install name_of_package
```

By this command you can too update specific package.

### Remove package

#### With dependiences

```bash
foxaur remove name_of_package
```

#### Without dependiences

```bash
foxaur remove --no-dependiences name_of_package
```

### Update all packages

```bash
foxaur update
```

### List installed packages

#### List all packages

```bash
foxaur list
```

#### List specific package

```bash
foxaur list name_of_package
```

## Contributing

If you want to contribute the project, check [CONTRIBUTING.md](https://gitlab.com/foxaur/foxaur/blob/master/CONTRIGUTING.md) file.

## Changelog

Check [CHANGELOG.md](https://gitlab.com/foxaur/foxaur/blob/master/CHANGELOG.md) to see changes in releases.

## Licence

This project is licenced under the [GNU General Public License v3.0](https://gitlab.com/foxaur/foxaur/blob/master/LICENSE).

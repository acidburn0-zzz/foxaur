import subprocess
import os
import shutil
import sys
import io

from .pacman import pacman_command


def clone_aur(package):
    global cache_directory
    global home_directory
    cache_directory = ".foxaur"
    home_directory = os.environ["HOME"]
    package_cache_directory = "%s/%s/%s" % (home_directory, cache_directory, package)
    if os.path.isdir(package_cache_directory):
        shutil.rmtree(package_cache_directory)
    command = "git clone https://aur.archlinux.org/" + package + ".git " + package_cache_directory
    with subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as aur:
        aur.communicate()
    if len(os.listdir(package_cache_directory)) == 1:
        shutil.rmtree(package_cache_directory)
        return False


def install_aur(package):
    package_cache_directory = "%s/%s/%s" % (home_directory, cache_directory, package)
    with subprocess.Popen("makepkg -s", shell=True, cwd=package_cache_directory) as aur:
        aur.communicate()
    pacman_command("-U", [package_cache_directory + "/*.pkg.tar.xz"])


def find_installed_aur():
    old_stdout = sys.stdout
    sys.stdout = io.StringIO()
    pacman_command("-Qqm")
    packages = sys.stdout.getvalue()
    sys.stdout.close()
    sys.stdout = old_stdout
    return packages.split()

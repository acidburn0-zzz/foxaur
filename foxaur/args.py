import argparse

from . import __version__


class Parser:

    @staticmethod
    def create_parser():
        parser = argparse.ArgumentParser(
            prog="foxaur",
            description="Pacman wrapper and AUR helper to keep it simple."
        )
        return parser

    def parse_args(self):
        parser = Parser.create_parser()
        parser.add_argument(
            '-v', "--version",
            action="version",
            version="%(prog)s " + __version__
        )
        sub_parser = parser.add_subparsers(dest="command")
        for command in ["install"]:
            sp = sub_parser.add_parser(command)
            sp.add_argument(
                'packages',
                type=str,
                nargs="+",
                help="Names of packages"
            )
        for command in ["remove"]:
            sp = sub_parser.add_parser(command)
            sp.add_argument(
                'packages',
                type=str,
                nargs="+",
                help="Names of packages"
            )
            sp.add_argument(
                "--no-dependiences",
                action="store_true",
                help="Do not remove dependiences"
            )
        for command in ["update"]:
            sub_parser.add_parser(command)
        for command in ["list"]:
            sp = sub_parser.add_parser(command)
            sp.add_argument(
                "packages",
                type=str,
                nargs="*",
                help="Names of packages"
            )
        return parser.parse_args(self)

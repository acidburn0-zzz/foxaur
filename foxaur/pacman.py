import subprocess


def pacman_command(flags, packages=[]):
    global return_code
    command = "pacman --noconfirm " + flags + " " + " ".join(packages)
    with subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as pacman:
        for line in pacman.stdout:
            print(line.decode(), end='')
        err = pacman.communicate()[1]
        if "root".encode() in err:
            sudo = True
        else:
            sudo = False
            return_code = pacman.returncode
    if sudo:
        with subprocess.Popen("sudo -S " + command, shell=True) as pacman:
            pacman.communicate()
            return_code = pacman.returncode
    return return_code

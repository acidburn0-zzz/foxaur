import sys

from .args import Parser
from .pacman import pacman_command
from .aur import (
    clone_aur,
    install_aur,
    find_installed_aur
)


def main():
    args = Parser.parse_args(sys.argv[1:])
    if args.command is None:
        raise ValueError("Invalid command")

    if "install" in args.command:
        exit_code = pacman_command("-S", args.packages)
        if exit_code is not 0:
            print("Pacman doesn't found packages, searching AUR")
            pacman_packages = []
            for package in args.packages:
                is_in_aur = clone_aur(package)
                if is_in_aur is False:
                    print(package + " not founded in AUR")
                    pacman_packages.append(package)
                else:
                    install_aur(package)
            if pacman_packages:
                pacman_command("-S", pacman_packages)

    if "remove" in args.command:
        if args.no_dependiences:
            pacman_command("-R", args.packages)
        else:
            pacman_command("-Rs", args.packages)

    if "update" in args.command:
        pacman_command("-Syu")
        aur_packages = find_installed_aur()
        if aur_packages:
            for package in aur_packages:
                is_in_aur = clone_aur(package)
                if is_in_aur is False:
                    print(package + " not founded in AUR")
                else:
                    install_aur(package)

    if "list" in args.command:
        if not args.packages:
            pacman_command("-Q")
        else:
            pacman_command("-Q", args.packages)

if __name__ == "__main__":
    main()

import unittest

from foxaur.pacman import pacman_command


class TestPacman(unittest.TestCase):

    def test_list_installed(self):
        self.assertEqual(0, pacman_command("-Q", ["pacman"]))

    def test_list_no_installed(self):
        self.assertEqual(1, pacman_command("-Q", ["test-package"]))

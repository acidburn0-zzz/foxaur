import unittest

from foxaur.args import Parser


class TestArgparse(unittest.TestCase):

    def test_install_one(self):
        args = Parser.parse_args(["install", "nano"])
        self.assertEqual("install", args.command)
        self.assertEqual(["nano"], args.packages)
        self.assertNotIn("nano", args.command)
        self.assertNotIn("install", args.packages)

    def test_install_multiple(self):
        args = Parser.parse_args(["install", "nano", "vim"])
        self.assertEqual("install", args.command)
        self.assertIn("nano", args.packages)
        self.assertIn("vim", args.packages)
        self.assertNotIn("nano", args.command)
        self.assertNotIn("vim", args.command)
        self.assertNotIn("install", args.packages)

    def test_invalid_argument(self):
        args = Parser.parse_args("")
        self.assertRaises(ValueError)

    def test_remove(self):
        args = Parser.parse_args(["remove", "nano"])
        self.assertEqual("remove", args.command)
        self.assertEqual(["nano"], args.packages)
        self.assertNotIn("nano", args.command)
        self.assertNotIn("install", args.packages)

    def test_remove_multiple(self):
        args = Parser.parse_args(["remove", "nano", "vim"])
        self.assertEqual("remove", args.command)
        self.assertIn("nano", args.packages)
        self.assertIn("vim", args.packages)
        self.assertNotIn("nano", args.command)
        self.assertNotIn("vim", args.command)
        self.assertNotIn("remove", args.packages)

    def test_remove_without_dependiences(self):
        args = Parser.parse_args(["remove", "--no-dependiences", "nano"])
        self.assertEqual("remove", args.command)
        self.assertTrue(args.no_dependiences)
        self.assertEqual(["nano"], args.packages)
        self.assertNotIn("nano", args.command)
        self.assertNotIn("--no-dependiences", args.command)
        self.assertNotIn("remove", args.packages)
        self.assertNotIn("--no-dependiences", args.packages)

    def test_upgrade(self):
        args = Parser.parse_args(["update"])
        self.assertEqual("update", args.command)

    def test_list_all_installed(self):
        args = Parser.parse_args(["list"])
        self.assertEqual("list", args.command)

    def test_list_one_installed(self):
        args = Parser.parse_args(["list", "nano"])
        self.assertEqual("list", args.command)
        self.assertEqual(["nano"], args.packages)
        self.assertNotIn("nano", args.command)
        self.assertNotIn("list", args.packages)

    def test_list_multiple_installed(self):
        args = Parser.parse_args(["list", "nano", "vim"])
        self.assertEqual("list", args.command)
        self.assertIn("nano", args.packages)
        self.assertIn("vim", args.packages)
        self.assertNotIn("nano", args.command)
        self.assertNotIn("vim", args.command)
        self.assertNotIn("list", args.packages)
